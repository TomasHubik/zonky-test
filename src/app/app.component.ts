import {Component, OnInit} from '@angular/core';
import {ApiService} from './services/api.service';
import {MarketplaceService} from './services/marketplace.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent implements OnInit {
  public fetching = false;
  private fetchedData; // Stored last fetched data for other possible usage
  public selectedRating = 'A'; // Default selected rating
  public amountAverage;
  public fetchSize = 200; // Size of fetched items later passed as X-Size in request header

  constructor(
    private marketPlaceService: MarketplaceService
  ) {}

  async ngOnInit() {
    this.fetchData(); // Call initial fetch
  }

  private fetchData() {
    this.fetching = true;

    this.marketPlaceService.getMarketplace(this.selectedRating, this.fetchSize).subscribe( // Call service to fetch data
      (response) => {
        this.fetchedData = response; // Store data for later usage
        this.amountAverage = this.fetchedData.reduce((total, next) => total + next.amount, 0) / this.fetchedData.length; // Calc average
        this.fetching = false;
      }, (error) => { // On any error dispaly alert message
        alert('There was error during fetching data:' + JSON.stringify(error));
        console.log(error);
        this.fetching = false;
      }
    );
  }
}
