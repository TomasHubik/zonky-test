import { Injectable } from '@angular/core';

import { HttpHeaders, HttpClient } from '@angular/common/http';
import {ApiService} from './api.service';

@Injectable()
export class MarketplaceService {
  constructor(
    private apiService: ApiService
  ) {}

  public getMarketplace(rating, fetchSize) { // Simple service to provide simple get call with header params
    const headerParams = {};
    headerParams['X-Size'] = String(fetchSize); // Additional header param to enlarge number of loans
    /*
      App use proxy to prevent CORS. Check proxy.conf.json to see remap of paths
    */
    return this.apiService.get('/api/loans/marketplace?rating__eq=' + rating, headerParams);
  }

}
