import { Injectable } from '@angular/core';

import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {
  constructor(
    private http: HttpClient
  ) {}

  public get(url: string, headerParams = {}) { // Simple service to provide simple get call with header params
    const headerSettings = {};
    headerSettings['Content-Type'] = 'application/json';
    const newHeader = new HttpHeaders(Object.assign(headerSettings, headerParams));
    return this.http.get(url, { headers: newHeader} );
  }

}
