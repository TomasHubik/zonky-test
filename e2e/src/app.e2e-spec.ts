import {browser, by, element} from 'protractor';
import {AppPage} from './app.po';


describe('Test API ->', () => {
  const page = AppPage;

  it('Check if API returns data:', async () => {
    browser.get(browser.baseUrl) as Promise<any>;

    element(by.cssContainingText('option', '4A')).click();
    browser.sleep(2000);
    const averageText = await element(by.id('resultTarget')).getText();
    expect(averageText.length).toBeGreaterThan(0);
  });

});
